## app analysis

#### Objective

Ta3limy is a platform to help students from all grade levels to study online in a fun and interesting way and also develop their intellectual skills. Ta3limy also is helping parents by providing them with the right courses to help in better educational experience and how to use technology in the right way.

####  Main Modules

+ Register ``User have the option to register as Student or Parent``
+ Login ``User have the option to login as Student or Parent``
> Parent Module:
+ My courses ``All courses Parent signed up for``
+ For you ``Courses and web pages for Parent to sign up or browse related to Parent``
+ For your child ``Courses and web pages for Parent to sign up or browse related to Student``
> Student Module:
+ My curriculum ``All curriculum for the specified grade``
+ My courses ``All courses Student signed up for``
+ Develop your skills ``Intellectual courses``
> Common Shared Module:
+ My lessons *Drousy Located on the navbar* ``Lists with active and finished courses``
+ Account data ``Web page with user data, can edit some field``
+ Logout
> General Web Pages:
+ FAQ
+ Report an issue
+ Tour icon
> Others:
+ UserWar Bot
+ Notification icon
