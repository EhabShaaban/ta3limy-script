## curriculum checklist

+ Click on any curriculum by *href Start Course* should redirect Student to curriculum content
+ Two blocks on the left should be existing
+ List of the curriculum should be displayed to the right
+ Student should not be able to go to exercise without finishing the previous one
+ By clicking on *Start now* Student should be redirected to execursie page
+ Exercise page should be displaying a video on the left
+ A list with chained videos to the same exercise should be on the right
+ Clicking on a video should redirect to the corresponding page
+ Go back to continue with the next locked exercises