## curriculum checklist execution

+ Click on any curriculum by *href Start Course* should redirect Student to curriculum content
> (Check) *No remarks*
+ Two blocks on the left should be existing
> (Check) *No remarks*
+ List of the curriculum should be displayed to the right
> (Check) *No remarks*
+ Student should not be able to go to exercise without finishing the previous one
> (Check) *No remarks*
+ By clicking on *Start now* Student should be redirected to execursie page
> (Check) *No remarks*
+ Exercise page should be displaying a video on the left
> (Check) *No remarks*
+ A list with chained videos to the same exercise should be on the right
> (Check) *No remarks*
+ Clicking on a video should redirect to the corresponding page
> (Check) *No remarks*
+ Go back to continue with the next locked exercises
> (Not check) All locked exercises will be unlocked ``Not Valid``
