## user role enhancement

+ When user navigates to https://www.ta3limy.com/register user can select to be Student and Parent at the same time
+ While it could be a more simple user experience to be as *Male* and *Female* buttons
+ To only allow user to only select one option
