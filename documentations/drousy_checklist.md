## drousy checklist

+ Active courses should be displayed under *My current courses*
+ Completed courses should be displayed under * Previous courses*
+ Toggle bar on the right should be existing with the following:
> *Drousy*, *Personal data* and *Logout*
+ Each option should redirect to the corresponding page
