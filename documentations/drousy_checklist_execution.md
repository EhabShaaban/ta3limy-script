## drousy checklist execution

+ Active courses should be displayed under *My current courses*
> (Check) *No remarks, clicking on course redirect to the course content*
+ Completed courses should be displayed under * Previous courses*
> (Check) *No remarks, clicking on course redirect to the course content*
+ Toggle bar on the right should be existing with the following:
> *Drousy*, *Personal data* and *Logout*
> (Check) *No remarks*
+ Each option should redirect to the corresponding page
> (Check) clicking on *drousy* redirect to the same page
> (Check) clicking on *Personal Data* redirect to Personal Data page
> (Check) clicking on *Log out* logged out the user from ta3limy
