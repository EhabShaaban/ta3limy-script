## register checklist

+ All mandatory fields should be validated and indicated by a red highlight surrounding textboxes or stating this under buttons
+ Validating that error msgs. should be displayed in the correct position
+ All error msgs. should be displayed with same CSS format *red in our case*
+ Check the text on the register page for spelling and grammatical errors
+ Check the functionality of buttons on the register page
+ Mobile number field should accept only numbers
+ confirmation password should be the same as password
+ User must click on terms and conditions check box
