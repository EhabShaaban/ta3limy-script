## register checklist execution

+ All mandatory fields should be validated and indicated by a red highlight surrounding textboxes or stating this under buttons
> (Check) *No remarks*
+ Validating that error msgs. should be displayed in the correct position
> (Check) *No remarks*
+ All error msgs. should be displayed with same CSS format *red in our case*
> (Check) *No remarks*
+ Check the text on the register page for spelling and grammatical errors
> (Check) *No remarks*
+ Check the functionality of buttons on the register page
> (Check) *No remarks*
+ Mobile number field should accept only numbers
> (Check) *No remarks*
+ Confirmation password should be the same as password, if not error should be raised
> (Check) *No remarks*
+ User must click on terms and conditions check box, if not error should be raised
> (Check) *No remarks*
